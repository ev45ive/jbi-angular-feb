# GIT
https://bitbucket.org/ev45ive/jbi-angular-feb/

git clone https://bitbucket.org/ev45ive/jbi-angular-feb/

# New project
cd ~/Desktop

mkdir angular-project

code angular-project

cd ..

npm i -g @angular/cli

ng new angular-project

https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

https://marketplace.visualstudio.com/items?itemName=johnpapa.Angular2

# Playlists

ng g m playlists --routing -m app

ng g c playlists/views/playlists-view --export

ng g c playlists/components/ItemsList
ng g c playlists/components/PlaylistDetails
ng g c playlists/components/PlaylistForm

ng g c playlists/views/playlists-view --export --force

# Bootstrap
npm i --save bootstrap 

# Shared

ng g m shared -m playlists


# NgProbe

ng.probe($0).providerTokens[2]

# Album search

ng g m search -m app --routing

ng g c search/views/search-view --export

.row*2>.col

ng g c search/components/search-form

ng g c search/components/result-grid
ng g c search/components/album-card

https://getbootstrap.com/docs/4.4/components/input-group/#button-addons

https://getbootstrap.com/docs/4.4/components/card/#card-groups

ng g s search/services/music-search


# Auth
https://github.com/manfredsteyer/angular-oauth2-oidc

ng g m security -m app
ng g s security/auth


# Feedback

http://jbinternational.co.uk/feedback/6356


https://www.linkedin.com/in/mateuszkulesza/