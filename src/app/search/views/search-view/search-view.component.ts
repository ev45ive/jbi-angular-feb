import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicSearchService } from "../../services/music-search.service";
import { Router, ActivatedRoute } from "@angular/router";
import { map, filter } from "rxjs/operators";
//  https://github.com/marak/Faker.js/
//  https://github.com/boo1ean/casual

@Component({
  selector: "app-search-view",
  templateUrl: "./search-view.component.html",
  styleUrls: ["./search-view.component.scss"]
})
export class SearchViewComponent implements OnInit {
  results: Album[] = [];
  message = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MusicSearchService
  ) {}

  ngOnInit() {
    // const query = this.route.snapshot.queryParamMap.get("query");

    // this.route.paramMap
    this.route.queryParamMap
      .pipe(
        map(p => p.get("query")),
        filter(p => !!p)
      )
      .subscribe(query => {
        this.fetchResults(query!);
      });
  }

  search(query: string) {
    this.router.navigate(["/search"], {
      queryParams: {
        query: query
      },
      replaceUrl: true
    });
  }

  private fetchResults(query: string) {
    this.service.getAlbums(query).subscribe({
      next: albums => (this.results = albums),
      error: err => {
        this.message = err.message;
      },
      complete: () => console.log("complete")
    });
  }
}
