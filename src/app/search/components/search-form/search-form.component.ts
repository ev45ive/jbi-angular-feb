import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { filter, distinctUntilChanged, debounceTime, tap } from "rxjs/operators";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm = new FormGroup({
    type: new FormControl("album"),
    query: new FormControl("batman")
  });

  constructor() {
    (window as any).form = this.queryForm;
  }

  @Output() searchChange = new EventEmitter<string>();

  ngOnInit() {
    const valueChanges = this.queryForm.get("query")!.valueChanges;

    const searchChanges = valueChanges.pipe(
      // send when stopped typing
      debounceTime(400),
      // >3 chars
      filter(q => q.length >= 3),
      // no duplicates
      distinctUntilChanged(),
      // tap(console.log),
    );

    searchChanges.subscribe((q: string) => this.searchChange.emit(q));
  }

  search() {
    this.searchChange.emit(
      //
      this.queryForm.get("query")!.value
    );
  }
}
