import { Component, OnInit, Input } from "@angular/core";
import { Album } from "src/app/models/Album";

@Component({
  selector: "app-result-grid",
  templateUrl: "./result-grid.component.html",
  styleUrls: ["./result-grid.component.scss"]
})
export class ResultGridComponent implements OnInit {

  @Input()
  results: Album[] = [];

  constructor() {}

  ngOnInit() {}
}
