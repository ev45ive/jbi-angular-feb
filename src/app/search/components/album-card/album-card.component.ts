import { Component, OnInit, HostBinding, Input } from "@angular/core";
import { Album } from "src/app/models/Album";

@Component({
  selector: "app-album-card, [app-album-card]",
  templateUrl: "./album-card.component.html",
  styleUrls: ["./album-card.component.scss"]
})
export class AlbumCardComponent {
  
  @Input() album!: Album;

  @HostBinding("class.card")
  card = true;

  constructor() {}

  ngOnInit() {}
}
