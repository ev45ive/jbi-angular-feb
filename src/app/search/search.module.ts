import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SearchRoutingModule } from "./search-routing.module";
import { SearchViewComponent } from "./views/search-view/search-view.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { ResultGridComponent } from "./components/result-grid/result-grid.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { API_URL } from "./API_URL";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    SearchViewComponent,
    SearchFormComponent,
    ResultGridComponent,
    AlbumCardComponent
  ],
  imports: [
    // HttpClientModule, 
    CommonModule, 
    SearchRoutingModule,
    SharedModule
  ],
  exports: [SearchViewComponent],
  providers: [
    // {
    //   provide: API_URL,
    //   useValue: environment.spotify_url
    // }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url: string) {
    //     return new MusicSearchService(url);
    //   },
    //   deps: [API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [API_URL]
    // },
    // MusicSearchService
  ]
})
export class SearchModule {}
