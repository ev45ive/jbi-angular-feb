import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthConfig {
  authUrl!: string; // https://accounts.spotify.com/authorize
  client_id!: string;
  redirect_uri!: string;
  response_type: "token" | "code" = "token";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    let token = sessionStorage.getItem("token");

    if (token) {
      token = JSON.parse(token);
    }

    if (!token) {
      token = this.extractToken();
      if (token) {
        location.hash = "";
        sessionStorage.setItem("token", JSON.stringify(token));
      }
    }

    this.token = token;
  }

  extractToken() {
    const p = new HttpParams({ fromString: location.hash.substr(1) });
    const access_token = p.get("access_token");
    return access_token;
  }

  authorize() {
    sessionStorage.removeItem("token");
    
    const { authUrl, client_id, redirect_uri, response_type } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type,
        show_dialog: "true"
      }
    });
    location.replace(authUrl + "?" + p.toString());
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
