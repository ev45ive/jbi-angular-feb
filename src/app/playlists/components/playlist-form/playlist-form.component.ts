import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent {
  // @ViewChild("formRef")

  // @ViewChild(NgForm, { static: true /* , read: NgForm */ })
  // formRef?: NgForm;

  // ngAfterViewInit() {
  //   setTimeout(() => {
  //     // this.formRef && this.formRef.setValue(this.playlist);
  //     this.formRef && this.formRef.form.patchValue(this.playlist);
  //   });
  //   // console.log(3, this.formRef);
  // }

  @Input()
  playlist!: Playlist;

  @Output() cancel = new EventEmitter();

  @Output() save = new EventEmitter<Playlist>();

  savePlaylist(ref: Partial<Playlist>) {
    const draft = {
      ...this.playlist,
      ...ref
    };
    this.save.emit(draft);
  }

  constructor() {}

  ngOnInit() {}
}

// type Partial<T> = {
//   [k in keyof T]?:  T[k]
// }


