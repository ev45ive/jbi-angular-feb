import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PlaylistsRoutingModule } from "./playlists-routing.module";
import { PlaylistsViewComponent } from "./views/playlists-view/playlists-view.component";
import { ItemsListComponent } from "./components/items-list/items-list.component";
import { PlaylistDetailsComponent } from "./components/playlist-details/playlist-details.component";
import { PlaylistFormComponent } from "./components/playlist-form/playlist-form.component";
import { ListItemComponent } from "./components/list-item/list-item.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    ItemsListComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    ListItemComponent
  ],
  imports: [CommonModule, PlaylistsRoutingModule, SharedModule],
  exports: [PlaylistsViewComponent]
})
export class PlaylistsModule {}
