// http://www.jsontots.com/
// https://marketplace.visualstudio.com/items?itemName=MariusAlchimavicius.json-to-ts
// https://www.npmjs.com/package/swagger-angular-generator

export interface Album {
  // type: "album";
  id: string;
  name: string;
  images: AlbumImage[];
  artists: Artist[];
}

export interface AlbumImage {
  url: string;
}

export interface Artist {
  // type: "artist";
  id: string;
  name: string;
}

export interface PagingObject<T> {
  items: T[];
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}

// const data: Album | Artist = {};

// switch (data.type) {
//   case "album":
//     {
//     }
//     break;
// }
