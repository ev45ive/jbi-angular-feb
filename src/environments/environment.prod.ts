import { AuthConfig } from "src/app/security/auth.service";

export const environment = {
  production: true,
  spotify_url: "https://api.spotify.com/v1/search",
  authConfig: {
    authUrl: "https://accounts.spotify.com/authorize",
    client_id: "70599ee5812a4a16abd861625a38f5a6",
    redirect_uri: "http://localhost:4200/",
    response_type: "token"
  } as AuthConfig
};
